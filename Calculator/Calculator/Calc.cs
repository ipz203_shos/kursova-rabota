﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Calculator
{
    class Calc
    {

        public double Factorial(double n)
        {
            int res = 1;
            for (int j = 1; j <= n; j++)
            {
                res *= j;
            }
            return res;
        }
        public double Sqrt(double num1)
        {
            return Math.Sqrt(num1);
        }
        public double pow_two(double num1)
        {
            return Math.Pow(num1,2);
        }
        public double pow_3(double num1)
        {
            return Math.Pow(num1, 3);
        }
        public double log(double num1)
        {
            return Math.Log10(num1);
        }
        public double ln(double num1)
        {
            return Math.Log(num1);
        }
        public double Sqrt_3(double num1)
        {
            return Math.Pow(num1, (double)1 / 3);
        }
        public double Powten_x(double num1)
        {
            return Math.Pow(10, num1);
        }
        public double sin(double num1)
        {
            return Math.Sin(num1);
        }
        public double cos(double num1)
        {
            return Math.Cos(num1);
        }
        public double tan(double num1)
        {
            return Math.Tan(num1);
        }
        public double memory_set(double memory)
        {
            return memory;
        }

    }
}
